# Module Cleanup

## Introduction

Lists uninstalled or deleted modules with leftover transient data and allows the admin to delete any leftover data. A fix for [Module 'module_name' has an entry in the system.schema key/value storage](https://www.drupal.org/node/3137656) and [The 'module_name' entity type does not exist](https://www.drupal.org/project/paragraphs/issues/3165612) error.

## Installation

Install with composer  
Enable module  
Go to Configuration->System->Delete transient module data  

## Maintainers

### Current maintainers

- [Trigve Hagen](https://www.drupal.org/u/trigve-hagen) 
